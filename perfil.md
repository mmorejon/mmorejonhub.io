---
layout: page
title: Perfil
permalink: /perfil/
translate_en: /en/profile/
---

## Información General
<p class="profile-description">Ingeniero DevOps & Consultor. Capitán de Docker.
<br>
Madrid | España
</p>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Actual:</td>
			<td class="profile-table-info">Ingeniero Cloud en <a href="https://sngular.com" target="_blank">Sngular</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Anterior:</td>
			<td class="profile-table-info">Ingeniero DevOps en <a href="https://www.mnemo.com" target="_blank">MNemo</a></td>
		</tr>
	</tbody>
</table>

## Certificaciones
<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Kubernetes:</td>
			<td class="profile-table-info"><a href="https://www.youracclaim.com/badges/2a75ba1d-9b4d-4a47-82c3-898c9bda122a" target="_blank"><img src="{{ site.baseurl }}/images/logo_cka.png" title="Certified Kubernetes Administrator" name="Certified Kubernetes Administrator" style="height: 170px;"/></a>
			</td>
			<td class="profile-table-header">DevOps:</td>
			<td class="profile-table-info"><a href="https://candidate.peoplecert.org/MobileReports.aspx?id=FCBBDF527E327965E4A3D66658F0F9570D5C8CAB4F7F9EEC9DABD944ED03A7ED0103CCE6EFA71F1BE137058D1C2FF130460E298BCEC17A0CD2CB5B15C122F93E06EF19E98588D394576B4E18F99AA929D1C1B8CFC0E2CA787D3378E4C302DEE66E2B28474FD63661F85CDE42529570C7B700B3975A53D0B44341D37082DDCA6B" target="_blank"><img src="{{ site.baseurl }}/images/devops-institute.jpeg" title="DevOps Foundation Certificate" name="DevOps Foundation Certificate" style="height: 170px;"/></a>
			</td>
		</tr>
	</tbody>
</table>

## Enfocado en

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Plataformas</td>
			<td class="profile-table-info">AWS y Digital Ocean</td>
		</tr>
		<tr>
			<td class="profile-table-header">Arquitecturas</td>
			<td class="profile-table-info">Microservicios y Severless</td>
		</tr>
		<tr>
			<td class="profile-table-header">Sistemas</td>
			<td class="profile-table-info">Docker, Kubernetes, Jenkins, Jenkins X y Tekton</td>
		</tr>
		<tr>
			<td class="profile-table-header">Lenguajes</td>
			<td class="profile-table-info">Python, Go y Groovy</td>
		</tr>
	</tbody>
</table>

## Comunidades

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Comunidad Docker:</td>
			<td class="profile-table-info"><a href="https://www.docker.com/captains/manuel-morej%C3%B3n" target="_blank">Miembro del Programa Capitanes de Docker</a>
			<br>
			Miembro del Programa Mentores
			<br>
			<a href="https://www.meetup.com/Docker-Havana/" target="_blank">Fundador y Co-Organizador del Meetup Docker-Havana</a>
			<br><br>
			</td>
		</tr>
		<tr>
			<td class="profile-table-header">Comunidad DZone</td>
			<td class="profile-table-info"><a href="https://dzone.com/users/1291603/manuel.morejon.85@gmail.com.html" target="_blank">Miembro del Programa MVB</a>
			<br>
			Co-Autor de la Guía <a href="https://dzone.com/guides/devops-continuous-delivery-and-automation" target="_blank">DevOps: Continuous Delivery and Automation</a>
			<br>
			Artículos publicados:<br>
			<a href="https://dzone.com/articles/devops-tips-to-select-your-hosted-continuous-integ" target="_blank">Selecting Your Local Continuous Integration Tool</a>
			<br>
			<a href="https://dzone.com/articles/build-your-pipeline-in-jenkins-20-as-code-with-ios" target="_blank">How to Build Your Pipeline in Jenkins 2.0 as Code for iOS9 and XCode7</a>
			<br>
			<a href="https://dzone.com/articles/microservices-an-example-with-docker-go-and-mongod" target="_blank">Microservices: An Example With Docker, Go, and MongoDB</a>
			<br><br>
			</td>
		</tr>
		<tr>
			<td class="profile-table-header">Comunidad Udemy</td>
			<td class="profile-table-info"><a href="https://www.udemy.com/integrando-docker-a-su-infraestrucutra-y-servicios" target="_blank">Curso Integrando Docker a su Infraestructura y Servicios</a>
			</td>
		</tr>
	</tbody>
</table>

## Experiencia Laboral

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2019 - actualidad</td>
		</tr>
		<tr>
			<td class="profile-table-header">Empleador :</td>
			<td class="profile-table-info">Sngular</td>
		</tr>
		<tr>
			<td class="profile-table-header">Cargo :</td>
			<td class="profile-table-info">Ingeniero Cloud</td>
		<tr>
		</tr>
			<td class="profile-table-header">Actividades :</td>
			<td class="profile-table-info">
				La unión entre el equipo de desarrollo y operaciones es posible.<br><br>
				Defino la infraestructura necesaria para el equipo y sus productos. Garantizo alta disponibilidad y tolerancia a fallos a través de la administración de clusters en diferentes plataformas.<br><br>
				Fortalezco la cultura DevOps en el equipo como filosofía de trabajo basándome en la buena comunicación entre todos.
			</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2018 - 2019</td>
		</tr>
		<tr>
			<td class="profile-table-header">Empleador :</td>
			<td class="profile-table-info">MNemo</td>
		</tr>
		<tr>
			<td class="profile-table-header">Cargo :</td>
			<td class="profile-table-info">Ingeniero DevOps</td>
		</tr>
		<tr>
			<td class="profile-table-header">Actividades :</td>
			<td class="profile-table-info">
				Como DevOps, defino los flujos de trabajo para optimizar el trabajo del equipo.<br><br>
				En la definición de workflows tomo en cuenta elementos como: integración continua, despliegues continuos, organización de tableros de trabajo basados en paradigmas ágiles, chatbots, pruebas automatizadas, monitoreo de sistemas y productos, métricas de productos y su posterior análisis.<br><br>
				Defino la infraestructura necesaria para el equipo y sus productos. Garantizo alta disponibilidad y tolerancia a fallos a través de la administración de clusters en diferentes plataformas.
			</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2014 - 2018 (4 años)</td>
		</tr>
		<tr>
			<td class="profile-table-header">Empleador :</td>
			<td class="profile-table-info">Freelance</td>
		</tr>
		<tr>
			<td class="profile-table-header">Cargo :</td>
			<td class="profile-table-info">Ingeniero DevOps y Consultor</td>
		</tr>
		<tr>
			<td class="profile-table-header">Actividades :</td>
			<td class="profile-table-info">Gestión de código fuente, control de cambios, liberación y despliegue de productos, integración continua y construcción de productos.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2009 - 2014 (5 años)</td>
		</tr>
		<tr>
			<td class="profile-table-header">Empleador :</td>
			<td class="profile-table-info"><a href="http://cujae.edu.cu/" target="_blank">Universidad Tecnológica de La Habana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Cargo :</td>
			<td class="profile-table-info">Gestor de Configuración de Software y Docente</td>
		</tr>
		<tr>
			<td class="profile-table-header">Actividades :</td>
			<td class="profile-table-info">Como Gestor de Configuración de Software:<br>Gestión de código fuente, control de cambios, liberación y despliegue de productos, integración continua y construcción de productos.<br><br>
			Como Docente:<br>Profesor Instructor de las asignaturas: Estructuras de Datos, Diseño y Programación Orientada a Objetos, Control de Versiones y Cambios.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Otros :</td>
			<td class="profile-table-info">Contrato por 7 meses en el 2012 y por 3 meses en el 2013 como Analista, Diseñador y Desarrollador de Software en Venezuela.</td>
		</tr>
	</tbody>
</table>

## Educación

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2018 - 2019</td>
		</tr>
		<tr>
			<td class="profile-table-header">Título :</td>
			<td class="profile-table-info">Máster en Marketing Digital</td>
		</tr>
		<tr>
			<td class="profile-table-header">Expedido por :</td>
			<td class="profile-table-info"><a href="https://www.eude.es" target="_blank">EUDE Business School</a></td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2009 - 2013</td>
		</tr>
		<tr>
			<td class="profile-table-header">Título :</td>
			<td class="profile-table-info">Máster en Informática Aplicada</td>
		</tr>
		<tr>
			<td class="profile-table-header">Expedido por :</td>
			<td class="profile-table-info"><a href="http://cujae.edu.cu/" target="_blank">Universidad Tecnológica de La Habana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Tema :</td>
			<td class="profile-table-info">Gestión de Configuración de Software en entornos de código abierto.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Fechas :</td>
			<td class="profile-table-info">2004 - 2009</td>
		</tr>
		<tr>
			<td class="profile-table-header">Título :</td>
			<td class="profile-table-info">Ingeniero Informático</td>
		</tr>
		<tr>
			<td class="profile-table-header">Expedido por :</td>
			<td class="profile-table-info"><a href="https://cujae.edu.cu/" target="_blank">Universidad Tecnológica de La Habana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Calificación :</td>
			<td class="profile-table-info">4.58 / 5.0</td>
		</tr>
	</tbody>
</table>

## Idiomas

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Nativo :</td>
			<td class="profile-table-info">Español</td>
		</tr>
		<tr>
			<td class="profile-table-header">Otros :</td>
			<td class="profile-table-info">Inglés</td>
		</tr>
	</tbody>
</table>