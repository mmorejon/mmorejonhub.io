---
layout: post_en
title: GitOps - Workshop 101 using Kubernetes and FluxCD
permalink: /en/blog/gitops-workshop-101-using-kubernetes-and-fluxcd/
translate_es: /blog/gitops-taller-101-utilizando-kubernetes-y-fluxcd/
lang: en
sidebar: yes
promo: yes
category: [article]
tags: [docker, kubernetes, gitops, devops, helm]
image: /images/gitops-workshop-101/flux-overview.svg
excerpt: Workshop to take the first steps in the GitOps work philosophy using Kubernetes and FluxCD.
---

![Gitops taller 101 con Sngular]({{ site.baseurl }}/images/gitops-workshop-101/sngular_wallpaper_03.webp "Gitops workshop 101 with Sngular")

## Workshop description

The workshop was organized by the [Sngular](https://www.sngular.com/) company as part of its *Knowledge Interchange Track (KIT)* program.

The objective of the workshop was to show the advantages of the GitOps work philosophy on the Kubernetes platform.

The systems used during the exercise were: [Git](https://git-scm.com/), [Terraform](https://www.terraform.io/), [Docker](https://www.docker.com/), [Flux CD](https://fluxcd.io/) y [Kubernetes](https://kubernetes.io/). The platform used to create the Kubernetes cluster was [Digital Ocean](https://www.digitalocean.com/), but [Minikube](https://minikube.sigs.k8s.io/) can also be used.

## Code repository

The code repository used can be accessed [through this link](https://github.com/mmorejon/gitops-get-started).

## Video

{% include youtube.html id='KlUjaPZrz9c' %}
