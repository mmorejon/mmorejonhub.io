---
layout: post_en
title: Docker Swarm with Docker Machine, Scripts
permalink: /en/blog/docker-swarm-with-docker-machine-scripts/
translate_es: /blog/docker-swarm-con-docker-machine-scripts/
lang: en
sidebar: yes
promo: yes
category: [article]
tags: [docker, swarm, machine]
image: /images/docker-swarm-with-docker-machine/social.jpg  
excerpt: Create a local cluster using Docker Swarm and Docker Machine allow us to explore the benefits of Docker without compromise the services running on production and without use infrastructure resources. Let's see some scripts that help us with local swarm management.
---

![Docker Swarm with Docker Machine, Scripts]({{ site.baseurl }}/images/docker-swarm-with-docker-machine/banner.jpg "Docker Swarm with Docker Machine, Scripts")

## Introduction

[Docker Swarm](https://docs.docker.com/engine/swarm/){:target="_blank"} and [Docker Machine](https://docs.docker.com/machine/){:target="_blank"} allow us to create a cluster to publish our services. This article will show you some scripts to help to create a cluster in a rapid and easy way taking into account the concepts associated to **Docker Swarm**.

### Requirements

Have [Docker](https://docs.docker.com/engine/installation/){:target="_blank"} and [Docker Machine](https://docs.docker.com/machine/install-machine/){:target="_blank"} installed.

### Environment settings

```
$ docker version
Client:
 Version:      17.09.0-ce
 API version:  1.32
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:42:18 2017
 OS/Arch:      linux/amd64

Server:
 Version:      17.09.0-ce
 API version:  1.32 (minimum version 1.12)
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:40:56 2017
 OS/Arch:      linux/amd64
 Experimental: false
```

```
$ docker-machine version
docker-machine version 0.12.2, build 9371605
```

## Source code

The source code used in this article is published on [GitHub](https://github.com/mmorejon/playing-docker-swarm-docker-machine){:target="_blank"}.

## Create Swarm

The swarm will be composed by:
* 3 nodes with the manager role.
* 3 nodes with the worker role.

The following steps were done to create the swarm:
* Create the nodes.
* Initialize swarm mode in the first node created (node1).
* Get the manager and worker tokens to including the others nodes.
* Join manger nodes into the swarm.
* Join worker nodes into the swarm.

The script used was the following:

```
#!/bin/bash

# Creating 6 nodes 
echo "### Creating nodes ..."
for c in {1..6} ; do
    docker-machine create -d virtualbox node$c
done

# Get IP from leader node
leader_ip=$(docker-machine ip node1)

# Init Docker Swarm mode
echo "### Initializing Swarm mode ..."
eval $(docker-machine env node1)
docker swarm init --advertise-addr $leader_ip

# Swarm tokens
manager_token=$(docker swarm join-token manager -q)
worker_token=$(docker swarm join-token worker -q)

# Joinig manager nodes
echo "### Joining manager modes ..."
for c in {2..3} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $manager_token $leader_ip:2377
done

# Join worker nodes
echo "### Joining worker modes ..."
for c in {4..6} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $worker_token $leader_ip:2377
done

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)
```

If you had cloned the repository, you can use the following command inside the root folder:

`$ ./scripts/swarm-create.sh`

## Start Swarm nodes

The configuration suggested in this article are used in our local machine, so it is possible that you need to turn off the machine sometimes and continues the tests later. When you turn on your machine to continue you should start each node, one by one, and this routine could be tedious.

To avoid spending time on these activities I suggest you the use of the following script:

```
#!/bin/bash

# Start nodes
echo "### Starting nodes ..."
for c in {1..6} ; do
    docker-machine start node$c
done

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)
```

If you had cloned the repository, you can use the following command inside the root folder:

`$ ./scripts/swarm-start.sh`

## Remove Swarm

During swarm testing, we can get to the point where we did some mistakes because of the lack of the total knowledge of the commands. This situation cannot be a problem because everything is happening in a local environment. You can always remove the swarm completely and start again in a clean configuration.

To remove completely the swarm you can use the following script:

```
#!/bin/bash

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)

# Remove nodes
echo "### Removing nodes ..."
for c in {1..6} ; do
    docker-machine rm node$c --force
done
```

If you had cloned the repository, you can use the following command inside the root folder:

`$ ./scripts/swarm-remove.sh`

## Naming nodes into the Swarm

Docker allow us modify the node role using the commands `promote` and `demote`.

Because of we are doing tests locally you will desire to realize a role changed many times so I recommend you do not set node names, like `manager-1` or `manager-2` because the role should be changed at any time. Instead of these names you should use names like `node1`, `node2` .... `nodeN` to avoid confusions.

If you desire to test this functionality you should use the following commands:
* `$ docker node promote NODE`: Promote a node to **manager** in the swarm.
* `$ docker node demote NODE`: Demote a node to a **worker** in the swarm.

There are multiple articles that talk about the topic of how should be named the nodes in the infrastructure. If you want to know about related topic use the following term [Cattle vs Pets](https://www.google.nl/search?dcr=0&source=hp&q=cattle+vs+pets&oq=cattle+vs+pets&gs_l=psy-ab.3..0j0i22i30k1l2.173.9592.0.12509.36.27.2.0.0.0.647.3745.4-3j4.7.0....0...1.1.64.psy-ab..30.6.2212...46j0i10k1j0i46k1.0.JhJlcMmJ39I){:target="blank"}.
