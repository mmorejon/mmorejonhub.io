---
layout: post
title: GitOps - Taller 101 utilizando Kubernetes y FluxCD
permalink: /blog/gitops-taller-101-utilizando-kubernetes-y-fluxcd/
translate_en: /en/blog/gitops-workshop-101-using-kubernetes-and-fluxcd/
lang: es
sidebar: yes
promo: yes
category: [articulo]
tags: [docker, kubernetes, gitops, devops, helm]
image: /images/gitops-workshop-101/flux-overview.svg
excerpt: Taller para dar los primeros pasos en la filosofía de trabajo GitOps utilizando Kubernetes y FluxCD.
---

![Gitops taller 101 con Sngular]({{ site.baseurl }}/images/gitops-workshop-101/sngular_wallpaper_03.webp "Gitops taller 101 con Sngular")

## Descripción del taller

El taller fue organizado por la empresa [Sngular](https://www.sngular.com/) como parte de su programa *Knowledge Interchange Track (KIT)*.

El objetivo del taller fue mostrar las ventajas de la filosofía de trabajo GitOps en la plataforma Kubernetes.

Los sistemas utilizados durante el ejercicio fueron: [Git](https://git-scm.com/), [Terraform](https://www.terraform.io/), [Flux CD](https://fluxcd.io/) y [Kubernetes](https://kubernetes.io/). La plataforma utilizada para crear el cluster de Kubernetes fue [Digital Ocean](https://www.digitalocean.com/), pero también puede utilizarse [Minikube](https://minikube.sigs.k8s.io/).

## Repositorio de código

El repositorio de código utilizado puede ser accedido a [través de este enlace](https://github.com/mmorejon/gitops-get-started/blob/master/docs/es/README.md).

## Video

{% include youtube.html id='KlUjaPZrz9c' %}
