---
layout: post
title: Docker Swarm con Docker Machine, Scripts
permalink: /blog/docker-swarm-con-docker-machine-scripts/
translate_en: /en/blog/docker-swarm-with-docker-machine-scripts/
lang: es
sidebar: yes
promo: yes
category: [articulo]
tags: [docker, swarm, machine]
image: /images/docker-swarm-with-docker-machine/social.jpg
excerpt: Crear un cluster local utilizando Docker Swarm y Docker Machine nos permitirá explorar los beneficios de Docker sin comprometer los servicios en producción y sin emplear recursos de nuestra infraestructura. Veamos entonces algunos scripts para ayudarnos a gestionar un swarm local.
---

![Docker Swarm con Docker Machine, Scripts]({{ site.baseurl }}/images/docker-swarm-with-docker-machine/banner.jpg "Docker Swarm con Docker Machine, Scripts")

## Introducción

[Docker Swarm](https://docs.docker.com/engine/swarm/){:target="_blank"} y [Docker Machine](https://docs.docker.com/machine/){:target="_blank"} nos permiten crear un cluster para publicar nuestros servicios. El presente artículo mostrará algunos scripts para ayudar a crear el cluster de forma sencilla y rápida teniendo en cuenta los conceptos de **Docker Swarm**.

### Requisitos

Tener instalado [Docker](https://docs.docker.com/engine/installation/){:target="_blank"} y [Docker Machine](https://docs.docker.com/machine/install-machine/){:target="_blank"}

### Configuraciones del entorno

```
$ docker version
Client:
 Version:      17.09.0-ce
 API version:  1.32
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:42:18 2017
 OS/Arch:      linux/amd64

Server:
 Version:      17.09.0-ce
 API version:  1.32 (minimum version 1.12)
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:40:56 2017
 OS/Arch:      linux/amd64
 Experimental: false
```

```
$ docker-machine version
docker-machine version 0.12.2, build 9371605
```

## Código fuente

El código fuente del artículo se encuentra público en [GitHub](https://github.com/mmorejon/playing-docker-swarm-docker-machine){:target="_blank"}.

## Crear Swarm

El Swarm estará compuesto por:
* 3 nodos con el rol manager
* 3 nodos con el rol worker
 
Para crear el swarm local vamos a realizar los siguientes pasos:
* Crear los nodos.
* Inicializar el modo swarm en el primer nodo creado (node1).
* Obtener los tokens de manager y worker para incluir los restantes nodos.
* Vincular los nodos manager.
* Vincular los nodos woker.

El script utilizado fue el siguiente:

```
#!/bin/bash

# Creating 6 nodes 
echo "### Creating nodes ..."
for c in {1..6} ; do
    docker-machine create -d virtualbox node$c
done

# Get IP from leader node
leader_ip=$(docker-machine ip node1)

# Init Docker Swarm mode
echo "### Initializing Swarm mode ..."
eval $(docker-machine env node1)
docker swarm init --advertise-addr $leader_ip

# Swarm tokens
manager_token=$(docker swarm join-token manager -q)
worker_token=$(docker swarm join-token worker -q)

# Joinig manager nodes
echo "### Joining manager modes ..."
for c in {2..3} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $manager_token $leader_ip:2377
done

# Join worker nodes
echo "### Joining worker modes ..."
for c in {4..6} ; do
    eval $(docker-machine env node$c)
    docker swarm join --token $worker_token $leader_ip:2377
done

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)
```

Si usted ha clonado el repositorio puede utilizar el siguiente comando dentro de la carpeta raíz:

`$ ./scripts/swarm-create.sh`

## Iniciar los nodos del swarm

Las configuraciones sugeridas en el artículo son realizadas en nuestra máquina local por lo que es muy posible que en ocasiones apague para continuar las pruebas un rato más tarde. Al regresar usted deberá iniciar los nodos uno a uno lo cual se puede convertirse en una tarea tediosa.

Para evitar emplear tiempo en estas actividades le suguiero utilizar el siguiente script:

```
#!/bin/bash

# Start nodes
echo "### Starting nodes ..."
for c in {1..6} ; do
    docker-machine start node$c
done

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)
```

Si usted ha clonado el repositorio puede utilizar el siguiente comando dentro de la carpeta raíz:

`$ ./scripts/swarm-start.sh`

## Eliminar el swarm

Durante la realización de pruebas al **swarm** podemos llegar al punto donde nos hemos equivocado mucho por no tener total conocimiento de los comandos. Esto no debe ser motivo de molestias pues todo se está realizando en un entorno controlado y local. Usted puede elimitar todo el swarm y comenzar con una configuración limpia.

Para eliminar completamente el swarm puede utilizar el siguiete script:

```
#!/bin/bash

# Clean Docker client environment
echo "### Cleaning Docker client environment ..."
eval $(docker-machine env -u)

# Remove nodes
echo "### Removing nodes ..."
for c in {1..6} ; do
    docker-machine rm node$c --force
done
```

Si usted ha clonado el repositorio puede utilizar el siguiente comando dentro de la carpeta raíz:

`$ ./scripts/swarm-remove.sh`

## Nombre para los nodos del swarm

Docker brinda la posibilidad de modificar el rol de cada nodo a través de los comandos `promote` y `demote`.

Como estamos realizando pruebas de forma local usted va a desear realizar este tipo de cambio en múltiples ocasiones por lo que le recomiendo no crear los nodos con nombres como `manager-1` o `worker-2` porque en algún momento pudieras cambiar el rol. En vez de estos nombres le recomiendo que utilice `node1`, `node2` ... `nodeN` para evitar confusiones.

Si desea probar esta funcionalidad debe hacer uso de los siguientes comandos:
* `$ docker node promote NODE`: Promueve un nodo a **manager** en el swarm.
* `$ docker node demote NODE`: Degrada un nodo a **worker** en el swarm.

Existen múltiples artículos que tratan sobre el tema de cómo nombrar los nodos en la infraestructura. Si desea conocer elementos relacionados puede consultar el término [Ganado vs Mascotas (Cattle vs Pets)](https://www.google.nl/search?dcr=0&source=hp&q=cattle+vs+pets&oq=cattle+vs+pets&gs_l=psy-ab.3..0j0i22i30k1l2.173.9592.0.12509.36.27.2.0.0.0.647.3745.4-3j4.7.0....0...1.1.64.psy-ab..30.6.2212...46j0i10k1j0i46k1.0.JhJlcMmJ39I){:target="blank"}.
