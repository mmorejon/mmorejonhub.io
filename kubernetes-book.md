---
layout: page
title: Libro de Kubernetes para aprender desde cero
permalink: /libro/erase-una-vez-kubernetes
sidebar: no
translate_en: /en/book/erase-una-vez-kubernetes
lang: es
category: [libro]
tags: [kubernetes, docker, libro]
image: /images/kubernetes-book/kubernetes-large.jpg
excerpt: "Libro en Español para aprender Kubernetes sin necesidad de conocimientos previos para ambos roles: desarrolladores y operaciones."
---

!["Libro Érase una vez Kubernetes"](/images/kubernetes-book/kubernetes-large.webp){: .center-image }

Este es un libro diseñado para aprender Kubernetes.

Si no tienes conocimientos previos en Kubernetes este libro te ayudará a dar los primeros pasos y luego muchos pasos más. Sin embargo, si tienes conocimientos previos podrás reafirmar tus conceptos y practicar tus habilidades con interesantes ejercicios.

El libro incluye ejercicios para realizar de forma individual y también un sistema de evaluación interno que le informará si su respuesta es correcta o no.

¡Todo en un mismo libro! ¿Qué estás esperando para tener el tuyo?

{% include button.html
    class="leanpub-button"
    href="leanpub.com/erase-una-vez-kubernetes"
    text="Leer el libro" %}

## Conocimientos previos necesarios

* Tener conocimientos sobre Docker en nivel intermedio.
* Estar familiarizado con los conocimientos básicos de los sistemas Linux, pero no necesita ser experto.
* Tener conocimientos básicos para trabajar con editores de texto como vi o vim, pero de no conocerlos puede utilizar alternativas como nano.

## Contenido del libro

Conozca las piezas que componen el libro. Entre todas conforman el entorno correcto para aprender Kubernetes de forma rápida.

### Entorno de trabajo local

El entorno de trabajo se desarrolla en el ordenador personal del lector. De esta forma se evita tener gastos de infraestructura durante el aprendizaje. Los sistemas necesarios para la puesta en marcha del cluster son: *VirtualBox* y *Vagrant*. La instalación de Kubernetes se realiza utilizando *Kubeadm*.

El repositorio utilizado para crear el cluster de Kubernetes se encuentra [público en GitHub](https://github.com/mmorejon/erase-una-vez-k8s).

### Capítulos ordenados por objetos de Kubernetes

Los capítulos en el libro se encuentran estructurados principalmente por objetos de Kubernetes. Cada objeto corresponde con un nuevo concepto y está orientado a resolver una necesidad. El orden en que se estudian estos objetos es importante porque la complejidad entre ellos varía y se debe ir desde los más simples hacia los más complejos.

### Imágenes de apoyo a los conceptos

Durante el proceso de aprendizaje es necesario apropiarse de los nuevos conceptos lo más rápido posible. Para garantizar esta velocidad se han incluido imágenes en cada capítulo que apoyen a la idea que se desea transmitir. Por ejemplo:

!["Vista de nodos en Kubernetes"](/images/kubernetes-book/nodes-in-kubernetes-cluster.svg){: .center-image }

### Ejercicios prácticos en el Terminal

Cada capítulo incluye múltiples ejemplos prácticos asociados al concepto desarrollado. El código fuente de todos los ejercicios se encuentra [público en GitHub](https://github.com/mmorejon/erase-una-vez-k8s).

{% include button.html
    class="leanpub-button"
    href="leanpub.com/erase-una-vez-kubernetes"
    text="Leer el libro" %}
