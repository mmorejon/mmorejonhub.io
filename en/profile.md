---
layout: page_en
title: Profile
permalink: /en/profile/
translate_es: /perfil/
---

## General Information
<p class="profile-description">DevOps Engineer & Consultant. Docker Captain.
<br>
Madrid | Spain
</p>
<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Current:</td>
			<td class="profile-table-info">Cloud Engineer at <a href="https://sngular.com" target="_blank">Sngular</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Before:</td>
			<td class="profile-table-info">DevOps Engineer at <a href="https://www.mnemo.com" target="_blank">MNemo</a></td>
		</tr>
	</tbody>
</table>

## Certifications
<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Kubernetes:</td>
			<td class="profile-table-info"><a href="https://www.youracclaim.com/badges/2a75ba1d-9b4d-4a47-82c3-898c9bda122a" target="_blank"><img src="{{ site.baseurl }}/images/logo_cka.png" title="Certified Kubernetes Administrator" name="Certified Kubernetes Administrator" style="height: 170px;"/></a>
			</td>
			<td class="profile-table-header">DevOps:</td>
			<td class="profile-table-info"><a href="https://candidate.peoplecert.org/MobileReports.aspx?id=FCBBDF527E327965E4A3D66658F0F9570D5C8CAB4F7F9EEC9DABD944ED03A7ED0103CCE6EFA71F1BE137058D1C2FF130460E298BCEC17A0CD2CB5B15C122F93E06EF19E98588D394576B4E18F99AA929D1C1B8CFC0E2CA787D3378E4C302DEE66E2B28474FD63661F85CDE42529570C7B700B3975A53D0B44341D37082DDCA6B" target="_blank"><img src="{{ site.baseurl }}/images/devops-institute.jpeg" title="DevOps Foundation Certificate" name="DevOps Foundation Certificate" style="height: 170px;"/></a>
			</td>
		</tr>
	</tbody>
</table>

## Focused on

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Platforms</td>
			<td class="profile-table-info">AWS and Digital Ocean</td>
		</tr>
		<tr>
			<td class="profile-table-header">Architectures</td>
			<td class="profile-table-info">Microservicios and Severless</td>
		</tr>
		<tr>
			<td class="profile-table-header">Systems</td>
			<td class="profile-table-info">Docker, Kubernetes, Jenkins, Jenkins X and Tekton</td>
		</tr>
		<tr>
			<td class="profile-table-header">Languages</td>
			<td class="profile-table-info">Python, Go and Groovy</td>
		</tr>
	</tbody>
</table>

## Communities

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Docker Community</td>
			<td class="profile-table-info"><a href="https://www.docker.com/captains/manuel-morej%C3%B3n" target="_blank">Member of Docker Captain Program</a>
			<br>
			Member of Docker Mentor Program
			<br>
			<a href="https://www.meetup.com/Docker-Havana/" target="_blank">Founder and Co-Organizer of Docker Havana Meetup</a>
			<br><br>
			</td>
		</tr>
		<tr>
			<td class="profile-table-header">DZone Community</td>
			<td class="profile-table-info"><a href="https://dzone.com/users/1291603/manuel.morejon.85@gmail.com.html" target="_blank">Member of Program MVB</a>
			<br>
			Co-Author of the Guide <a href="https://dzone.com/guides/devops-continuous-delivery-and-automation" target="_blank">DevOps: Continuous Delivery and Automation</a>
			<br>
			Articles published:<br>
			<a href="https://dzone.com/articles/devops-tips-to-select-your-hosted-continuous-integ" target="_blank">Selecting Your Local Continuous Integration Tool</a>
			<br>
			<a href="https://dzone.com/articles/build-your-pipeline-in-jenkins-20-as-code-with-ios" target="_blank">How to Build Your Pipeline in Jenkins 2.0 as Code for iOS9 and XCode7</a>
			<br>
			<a href="https://dzone.com/articles/microservices-an-example-with-docker-go-and-mongod" target="_blank">Microservices: An Example With Docker, Go, and MongoDB</a>
			<br><br>
			</td>
		</tr>
		<tr>
			<td class="profile-table-header">Udemy Community</td>
			<td class="profile-table-info"><a href="https://www.udemy.com/integrando-docker-a-su-infraestrucutra-y-servicios" target="_blank">Course Integrating Docker into your infrastructure and services</a>
			</td>
		</tr>
	</tbody>
</table>

## Professional Experience

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2019 - current</td>
		</tr>
		<tr>
			<td class="profile-table-header">Employer :</td>
			<td class="profile-table-info">Sngular</td>
		</tr>
		<tr>
			<td class="profile-table-header">Job or Position :</td>
			<td class="profile-table-info">Cloud Engineer</td>
		</tr>
		<tr>
			<td class="profile-table-header">Activities :</td>
			<td class="profile-table-info">
				The union between the development team and operations is possible.<br><br>
				I define the necessary infrastructure for the equipment and its products. I guarantee high availability and fault tolerance through the administration of clusters in different platforms.<br><br>
				I strengthen the DevOps culture in the team as a work philosophy based on good communication among all.
			</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2018 - 2019</td>
		</tr>
		<tr>
			<td class="profile-table-header">Employer :</td>
			<td class="profile-table-info">MNemo</td>
		</tr>
		<tr>
			<td class="profile-table-header">Job or Position :</td>
			<td class="profile-table-info">DevOps Engineer</td>
		</tr>
		<tr>
			<td class="profile-table-header">Activities :</td>
			<td class="profile-table-info">
				As DevOps, I define the workflows to improve the work of the team.<br><br>
				In the definition of workflows, I take into account elements such as continuous integration, continuous delivery, continuous deployments, agile paradigms, chatbots, automated tests, monitoring of systems and products, metrics to products and their subsequent analysis.<br><br>
				I define the necessary infrastructure for the equipment and its products. I guarantee high availability and fault tolerance through the management of clusters on different platform platforms.
			</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2014 - 2018 (4 years)</td>
		</tr>
		<tr>
			<td class="profile-table-header">Employer :</td>
			<td class="profile-table-info">Freelance</td>
		</tr>
		<tr>
			<td class="profile-table-header">Job or Position :</td>
			<td class="profile-table-info">Software Configuration Management</td>
		<tr>
		</tr>
			<td class="profile-table-header">Activities :</td>
			<td class="profile-table-info">Source code management, change control, release and deploy management, continuous integration and build engineer.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2009 - 2014 (5 years)</td>
		</tr>
		<tr>
			<td class="profile-table-header">Employer :</td>
			<td class="profile-table-info"><a href="https://cujae.edu.cu/" target="_blank">Technological University of Havana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Job or Position :</td>
			<td class="profile-table-info">Software Configuration Management and Professor Instructor</td>
		<tr>
		</tr>
			<td class="profile-table-header">Activities :</td>
			<td class="profile-table-info">As Software Configuration Management:
			<br>Source code management, change control, release and deploy management, continuous integration and build engineer.
			<br><br>
			As Professor Instructor:
			<br>
			Professor Instructor of the followings subjects: Data Structure, Object Oriented Design and Programing, Software Versioning Control.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Others :</td>
			<td class="profile-table-info">Contracted as software analyst & designer in Venezuela’s Ministry of Education in 2012 and 2013.</td>
		</tr>
	</tbody>
</table>

## Education

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2018 - 2019</td>
		</tr>
		<tr>
			<td class="profile-table-header">Title :</td>
			<td class="profile-table-info">Master of Digital Marketing</td>
		</tr>
		<tr>
			<td class="profile-table-header">Issued by :</td>
			<td class="profile-table-info"><a href="https://www.eude.es" target="_blank">EUDE Business School</a></td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2009 - 2013</td>
		</tr>
		<tr>
			<td class="profile-table-header">Title :</td>
			<td class="profile-table-info">Master in Applied Software</td>
		</tr>
		<tr>
			<td class="profile-table-header">Issued by :</td>
			<td class="profile-table-info"><a href="https://cujae.edu.cu/" target="_blank">Technological University of Havana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Subject :</td>
			<td class="profile-table-info">Software Configuration Management in open source environments.</td>
		</tr>
	</tbody>
</table>

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Dates :</td>
			<td class="profile-table-info">2004 - 2009</td>
		</tr>
		<tr>
			<td class="profile-table-header">Title :</td>
			<td class="profile-table-info">Software Engineer</td>
		</tr>
		<tr>
			<td class="profile-table-header">Issued by :</td>
			<td class="profile-table-info"><a href="https://cujae.edu.cu/" target="_blank">Technological University of Havana</a></td>
		</tr>
		<tr>
			<td class="profile-table-header">Qualification :</td>
			<td class="profile-table-info">4.58 / 5.0</td>
		</tr>
	</tbody>
</table>

## Languages

<table class="profile-table">
	<tbody>
		<tr>
			<td class="profile-table-header">Mother Language :</td>
			<td class="profile-table-info">Spanish</td>
		</tr>
		<tr>
			<td class="profile-table-header">Others :</td>
			<td class="profile-table-info">English</td>
		</tr>
	</tbody>
</table>