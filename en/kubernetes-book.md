---
layout: page_en
title: Kubernetes book to learn from scratch
permalink: /en/book/erase-una-vez-kubernetes
sidebar: no
translate_es: /libro/erase-una-vez-kubernetes
lang: en
category: [book]
tags: [kubernetes, docker, book]
image: /images/kubernetes-book/kubernetes-large.jpg
excerpt: "Book in Spanish to learn Kubernetes without prior knowledge for both roles: developers and operations."
---

!["Book Érase una vez Kubernetes"](/images/kubernetes-book/kubernetes-large.webp){: .center-image }

This is a book designed to learn Kubernetes.

If you do not have previous knowledge in Kubernetes, this book will help you take the first steps and then many more steps. However, if you have previous knowledge you can reaffirm your concepts and practice your skills with interesting exercises.

The book includes exercises to be done individually and also an internal evaluation system that will inform you if your answer is correct or not.

All in one book! What are you waiting for to have yours?

{% include button.html
    class="leanpub-button"
    href="leanpub.com/erase-una-vez-kubernetes"
    text="Read the book" %}

## Necessary prior knowledge

* Have intermediate knowledge of Docker.
* Be familiar with the basic knowledge of Linux systems, but you don't need to be an expert.
* Have basic knowledge to work with text editors like vi or vim, but if you don't know them you can use alternatives like nano.

## Content of the book

Get to know the pieces that make up the book. Together they make up the right environment for learning Kubernetes quickly.

### Local work environment

The work environment is developed on the reader's personal computer. This avoids having infrastructure costs during learning. The necessary systems for the start-up of the cluster are: *VirtualBox* and *Vagrant*. The Kubernetes installation is done using *Kubeadm*.

The repository used to create the Kubernetes cluster is located [public on GitHub](https://github.com/mmorejon/erase-una-vez-k8s).

### Chapters sorted by Kubernetes objects

The chapters in the book are structured principally by Kubernetes objects. Each object corresponds to a new concept and is aimed at solving a need. The order in which these objects are studied is important because the complexity between them varies and you should go from the simplest to the most complex.

### Images to support the concepts

During the learning process it is necessary to appropriate new concepts as quickly as possible. To guarantee this speed, images have been included in each chapter that support the idea to be transmitted. For example:

!["Kubernetes nodes view"](/images/kubernetes-book/nodes-in-kubernetes-cluster.svg){: .center-image }

### Practical exercises in the Terminal

Each chapter includes multiple practical examples associated with the developed concept. The source code for all exercises is [public on GitHub](https://github.com/mmorejon/erase-una-vez-k8s).

{% include button.html
    class="leanpub-button"
    href="leanpub.com/erase-una-vez-kubernetes"
    text="Read the book" %}
